﻿using System.Web.Mvc;
using Library.DomainServices.Interfaces;
using Library.Dto;
using Library.Entity.Interfaces;

namespace Library.Controllers
{
    public class BaseController<TDomainService, TViewModel, TEntity, TDto> : Controller
        where TEntity : class
        where TDto : class
        where TDomainService : IBaseDomainService<TEntity>, new()
        where TViewModel : IBaseViewModel<TDto>, new()
    {
        [HttpPost]
        public void Remove(long id)
        {
            new TDomainService().Remove(id);
        }

        [HttpPost]
        public void Edit(TEntity entity)
        {
            new TDomainService().Edit(entity);
        }

        [HttpPost]
        public void Create(TEntity entity)
        {
            new TDomainService().Save(entity);
        }

        public JsonResult List(string search)
        {
            var list = new TViewModel().List(search);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Detail(long id)
        {
            var entity = new TViewModel().Get(id);
            return Json(entity, JsonRequestBehavior.AllowGet);
        }
    }
}