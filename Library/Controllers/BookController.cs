﻿using System.Web.Mvc;
using Library.DomainServices;
using Library.Dto;
using Library.Entity;
using Library.ViewModels;

namespace Library.Controllers
{
    public class BookController : BaseController<BookDomainService, BookViewModel, Book, BookDto>
    {
        [HttpPost]
        public JsonResult GetBPublishingDropDown(string filter)
        {
            var list = new BookPublicshingViewModel().GetForDropDown(filter);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}