﻿using System;
using System.Data.Entity;
using System.Linq;
using Library.DomainServices.Interfaces;
using Library.Entity;
using Library.Entity.Interfaces;

namespace Library.DomainServices
{
    public abstract class BaseDomainService<TEntity> : IBaseDomainService<TEntity> where TEntity : BaseEntity, IHasId
    {
        private DBContext DbContext { get; set; } = new DBContext();

        private IDbSet<TEntity> DbSet => DbContext.Set<TEntity>();

        public virtual IQueryable<TEntity> GetAll()
        {
            return DbContext.Set<TEntity>().Where(entity => !entity.Deleted);
        }

        public virtual void Save(TEntity entity)
        {
            Save(new[] {entity});
        }

        public virtual void Save(params TEntity[] entities)
        {
            foreach (var entity in entities)
                DbSet.Add(entity);
            DbContext.SaveChanges();
        }

        public virtual TEntity Get(long id)
        {
            return DbSet.Single(entity => entity.Id == id);
        }

        public virtual void Remove(long id)
        {
            var en = Get(id);
            if (en == null)
                return;
            DbSet.Remove(en);
            DbContext.SaveChanges();
        }

        public virtual void Delete(long id)
        {
            var en = Get(id);
            if (en == null)
                return;
            en.Deleted = true;
            DbContext.Entry(en).State = EntityState.Modified;
            DbContext.SaveChanges();
        }

        public void Edit(TEntity entity)
        {
            Edit(new[] {entity});
        }

        private void EditParams(TEntity entity)
        {
            var savedEntity = Get(entity.Id);
            if (savedEntity == null) return;

            var properties = typeof(TEntity).GetProperties();
            foreach (var property in properties)
            {
                var newValue = property.GetValue(entity);
                property.SetValue(savedEntity, newValue);
            }
            DbContext.Entry(savedEntity).State = EntityState.Modified;
        }

        public void Edit(params TEntity[] entities)
        {
            foreach (var entity in entities)
            {
                EditParams(entity);
            }
            DbContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposing)
                return;
            if (DbContext == null)
                return;
            DbContext.Dispose();
            DbContext = null;
        }
    }
}