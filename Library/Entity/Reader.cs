﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Library.Entity.Interfaces;

namespace Library.Entity
{
    /// <summary> Читатель </summary>
    public class Reader : BaseEntity
    {
        /// <summary> Дата рождения </summary>
        [Display(Name = "Дата рождения")]
        public DateTime DateBirth { get; set; }
        
        // <summary> Список прокатов </summary>
        [Display(Name = "Список прокатов")]
        public virtual ICollection<Order> Orders { get; set; } = new List<Order>();
    }
}