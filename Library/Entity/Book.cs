﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Library.Entity
{
    /// <summary> Книга </summary>
    public class Book : BaseEntity
    {
        /// <summary> Артикул </summary>
        [Display(Name = "Артикул")]
        public string Vendor { get; set; }

        /// <summary> Автор </summary>
        [Display(Name = "Автор")]
        public string Author { get; set; }

        /// <summary> Год издания </summary>
        [Display(Name = "Год издания")]
        public string Year { get; set; }
        
        /// <summary> Идентификатор издателя </summary>
        public long BookPublicshingId { get; set; }

        /// <summary> Издатель </summary>
        [Display(Name = "Издатель")]
        public virtual BookPublicshing BookPublicshing { get; set; }
        
        // <summary> Список пользования </summary>
        [Display(Name = "Список пользования")]
        public virtual ICollection<Order> Orders { get; set; } = new List<Order>();
    }
}