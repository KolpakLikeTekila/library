﻿namespace Library.Entity.Interfaces
{
    public interface IHasId
    {
        long Id { get; set; }
    }
}