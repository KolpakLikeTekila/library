﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Library.Entity
{
    /// <summary> Издательство </summary>
    public class BookPublicshing : BaseEntity
    {
        /// <summary> Сокращенное наименование </summary>
        [Display(Name = "Сокращенное наименование")]
        public string ShortName { get; set; }

        /// <summary> Адрес издательства </summary>
        [Display(Name = "Адрес издательства")]
        public string Adress { get; set; }
        
        // <summary> Изданные книги </summary>
        [Display(Name = "Изданные книги")]
        public virtual ICollection<Book> Books { get; set; } = new List<Book>();

    }
}