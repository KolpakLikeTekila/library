﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Library.Entity
{
    /// <summary> Прокат книги </summary>
    public class Order
    {
        /// <summary> Дата начала проката </summary>
        [Display(Name = "Дата начала проката")]
        public DateTime DateStart { get; set; }
        
        /// <summary> Дата возрата</summary>
        [Display(Name = "Дата возрата")]
        public DateTime? DateEnd { get; set; }
        
        /// <summary> Даты продления</summary>
        [Display(Name = "Даты продления")]
        public List<DateTime> CheckPoint { get; set; }
        
        /// <summary> Идентификатор Читателя </summary>
        public long ReaderId { get; set; }

        /// <summary> Читатель </summary>
        [Display(Name = "Читатель")]
        public virtual Reader Reader { get; set; }
        
        /// <summary> Идентификатор Книги </summary>
        public long BookId { get; set; }

        /// <summary> Книга </summary>
        [Display(Name = "Книга")]
        public virtual Book Book { get; set; }
    }
}