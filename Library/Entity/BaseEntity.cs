﻿using System.ComponentModel.DataAnnotations;
using Library.Entity.Interfaces;

namespace Library.Entity
{
    /// <summary> Общие свойства сущностей </summary>
    public class BaseEntity: IHasId
    {
        /// <summary> Идентификатор </summary>
        public long Id { get; set; }

        /// <summary> Полное наименование </summary>
        [Display(Name = "Название")]
        public string Name { get; set; }
        
        public bool Deleted { get; set; }
    }
}