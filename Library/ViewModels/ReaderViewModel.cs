﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.DomainServices;
using Library.Dto;
using Library.Entity.Interfaces;

namespace Library.ViewModels
{
    public class ReaderViewModel : IBaseViewModel<ReaderDto>
    {
        public List<ReaderDto> List(string search)
        {
            var list = new List<ReaderDto>();
            using (var readerDS = new ReaderDomainService())
            {
                var readersQueryable = readerDS
                    .GetAll()
                    .Select(reader => new
                    {
                        Id = reader.Id,
                        DateBirth = reader.DateBirth,
                        Name = reader.Name
                    });

                if (!string.IsNullOrEmpty(search))
                {
                    readersQueryable = readersQueryable.Where(reader => reader.Name.ToLower()
                        .Contains(search.ToLower()));
                }
                list = readersQueryable.ToList()
                    .Select(reader => new ReaderDto()
                    {
                        Id = reader.Id,
                        DateBirth = reader.DateBirth.ToString("d"),
                        Name = reader.Name
                    }).ToList();
            }
            return list;
        }

        public ReaderDto Get(long id)
        {
            using (var readerDS = new ReaderDomainService())
            {
                return readerDS
                    .GetAll()
                    .Where(reader => reader.Id == id)
                    .Select(reader => new
                    {
                        Id = reader.Id,
                        DateBirth = reader.DateBirth,
                        Name = reader.Name
                    })
                    .ToList()
                    .Select(reader =>
                        new ReaderDto()
                        {
                            Id = reader.Id,
                            Name = reader.Name,
                            DateBirth = reader.DateBirth.ToString("yyyy-MM-dd HH':'mm':'ss")
                        })
                    .Single();
            }
        }
    }
}