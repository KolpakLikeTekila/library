﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.DomainServices;
using Library.Dto;
using Library.Entity.Interfaces;

namespace Library.ViewModels
{
    public class BookViewModel : IBaseViewModel<BookDto>
    {
        public List<BookDto> List(string search)
        {
            var list = new List<BookDto>();
            using (var bookDs = new BookDomainService())
            {
                var bookQueryable = bookDs
                    .GetAll()
                    .Select(book => new BookDto
                    {
                        Id = book.Id,
                        Name = book.Name,
                        Author = book.Author,
                        Vendor = book.Vendor,
                        Year = book.Year,
                        BookPublicshingId = book.BookPublicshingId,
                        Publicshin = book.BookPublicshing.Name
                    });

                if (!string.IsNullOrEmpty(search))
                {
                    bookQueryable = bookQueryable.Where(bp => bp.Name.ToLower().Contains(search.ToLower()));
                }
                list = bookQueryable.ToList();
            }
            return list;
        }

        public BookDto Get(long id)
        {
            using (var bookDS = new BookDomainService())
            {
                return bookDS
                    .GetAll()
                    .Where(book => book.Id == id)
                    .Select(book => new BookDto
                    {
                        Id = book.Id,
                        Name = book.Name,
                        Author = book.Author,
                        Vendor = book.Vendor,
                        Year = book.Year
                    })
                    .Single();
            }
        }
    }
}