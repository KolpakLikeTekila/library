﻿using System.Collections.Generic;

namespace Library.Entity.Interfaces
{
    public interface IBaseViewModel<TDto> where TDto : class
    {
        List<TDto> List(string search);

        TDto Get(long id);
    }
}