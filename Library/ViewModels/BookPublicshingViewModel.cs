﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.DomainServices;
using Library.Dto;
using Library.Entity.Interfaces;

namespace Library.ViewModels
{
    public class BookPublicshingViewModel : IBaseViewModel<BookPublicshingDto>
    {
        public List<BookPublicshingDto> List(string search)
        {
            var list = new List<BookPublicshingDto>();
            using (var bookPublicshingDs = new BookPublicshingDomainService())
            {
                var bookPublicshingQueryable = bookPublicshingDs
                    .GetAll()
                    .Select(bp => new BookPublicshingDto
                    {
                        Id = bp.Id,
                        Name = bp.Name,
                        ShortName = bp.ShortName,
                        Adress = bp.Adress
                    });

                if (!string.IsNullOrEmpty(search))
                {
                    bookPublicshingQueryable = bookPublicshingQueryable.Where(bp => bp.Name.ToLower()
                                                                                        .Contains(search.ToLower()) ||
                                                                                    bp.ShortName.ToLower()
                                                                                        .Contains(search.ToLower()) ||
                                                                                    bp.Adress.ToLower()
                                                                                        .Contains(search.ToLower()));
                }
                list = bookPublicshingQueryable.ToList();
            }
            return list;
        }

        public BookPublicshingDto Get(long id)
        {
            using (var bookPublicshingDs = new BookPublicshingDomainService())
            {
                return bookPublicshingDs
                    .GetAll()
                    .Where(bp => bp.Id == id)
                    .Select(bp => new BookPublicshingDto
                    {
                        Id = bp.Id,
                        Name = bp.Name,
                        ShortName = bp.ShortName,
                        Adress = bp.Adress
                    })
                    .Single();
            }
        }

        public List<DropDownSelect> GetForDropDown(string filter)
        {
            var list = new List<DropDownSelect>();
            using (var bookPublicshingDs = new BookPublicshingDomainService())
            {
                var bookPublicshingQueryable = bookPublicshingDs
                    .GetAll()
                    .Select(bp => new DropDownSelect
                    {
                        value = bp.Id,
                        label = bp.Name
                    });

                if (!string.IsNullOrEmpty(filter))
                {
                    bookPublicshingQueryable = bookPublicshingQueryable.Where(bp => bp.label.ToLower()
                        .Contains(filter.ToLower()));
                }
                list = bookPublicshingQueryable.ToList();
            }
            return list;
        }
    }
}