import {Dispatcher} from 'flux';

class AppDispatcher extends Dispatcher {

    dispatchAsync(type, payload){
        this.dispatch({ type: type, payload:  payload});
    }
}

export default new AppDispatcher();
