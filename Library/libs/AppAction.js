import AppDispatcher from "./AppDispatcher"
import constants from "./constants";

let AddAction = {

    List(controllerName, params) {
        var url = controllerName + '/list';
        if (!params) {
            params = {};
        }
        $.get(url, params, function (data) {
            AppDispatcher.dispatchAsync(constants.dispatch_list, data);
        });
    },

};

export default AddAction;
