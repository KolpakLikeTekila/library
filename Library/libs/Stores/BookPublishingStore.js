import AppDispatcher from "../AppDispatcher";
import constants from "../constants";
import {ReduceStore} from 'flux/utils';

class BookPublishingStore extends ReduceStore {
    
    getInitialState() {
        return [];
    }

    reduce(state, action) {
        switch (action.type) {
            case constants.dispatch_list:
                return action.payload;
            default:
                return state;
        }
    }
}

export default new BookPublishingStore(AppDispatcher);
