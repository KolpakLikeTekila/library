﻿import React from 'react';
import {render} from 'react-dom';
import MainContainer from "./Components/MainContainer";
import constants from "./constants";
import Books from "./Components/Books/Books";
import Readers from "./Components/Readers/Readers";
import BookPublishings from "./Components/BookPublishings/BookPublishings";
import {BrowserRouter, Route, Switch} from 'react-router-dom';

render(
    <BrowserRouter>
        <Switch>
            <MainContainer>
                <Route path={`/${constants.path_book}`} component={Books}/>
                <Route path={`/${constants.path_reader}`} component={Readers}/>
                <Route path={`/${constants.path_publishing_house}`} component={BookPublishings}/>
            </MainContainer>
        </Switch>

    </BrowserRouter>,
    document.getElementById('root')
);