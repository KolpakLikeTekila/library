export default {
    path_book: 'book',
    path_reader: 'reader',
    path_publishing_house: 'publishing_house',

    dispatch_create: 'create',
    dispatch_update: 'update',
    dispatch_remove: 'remove',
    dispatch_list: 'list',
    dispatch_clear_store: 'clear'
}