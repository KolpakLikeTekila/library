import React, {Component} from 'react';
import constants from "../constants";
import {MenuItem, Nav, Navbar, NavDropdown, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

class MainContainer extends Component {
    render() {
        return (
            <div>
                <Navbar>
                    <Nav>
                        <LinkContainer to='#'>
                            <NavItem>Выдача книг</NavItem>
                        </LinkContainer>
                        <NavDropdown title="Справочники" id="basic-nav-dropdown">
                            <LinkContainer to={`/${constants.path_book}`}>
                                <MenuItem>Книги</MenuItem>
                            </LinkContainer>
                            <LinkContainer to={`/${constants.path_publishing_house}`}>
                                <MenuItem>Издательства</MenuItem>
                            </LinkContainer>
                            <LinkContainer to={`/${constants.path_reader}`}>
                                <MenuItem>Читатели</MenuItem>
                            </LinkContainer>
                        </NavDropdown>
                    </Nav>
                </Navbar>
                <div className='container'>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default MainContainer;