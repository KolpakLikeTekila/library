import React, {Component,} from 'react';
import EditModal from "../Base/EditModal";
import AppAction from "../../AppAction";
import BookFormField from "./BookFormField";

class Book extends Component {

    removeRider() {
        var url = this.props.controllerName + '/remove';
        var params = {id: this.props.Id};
        var scope = this;
        $.post(url, params, function (data) {
            var filter = scope.props.search();
            AppAction.List(this.props.controllerName, filter);
        });
    }

    render() {
        return (
            <tr>
                <td>{this.props.Name}</td>
                <td>{this.props.Vender}</td>
                <td>{this.props.Author}</td>
                <td>{this.props.Year}</td>
                <td>
                    <button title="Edit" className="btn btn-primary" onClick={() => this.refs.modalWindow.handleOpen()}>
                        Изменить
                    </button>
                    <button title="Delete" className="btn btn-danger" onClick={this.removeRider.bind(this)}>
                        Удалить
                    </button>
                </td>
                <EditModal ref="modalWindow"
                           buttonAction="Изменить"
                           title="Редактор книги"
                           controllerName={this.props.controllerName}
                           Id={this.props.Id}
                           getFilter={this.props.search}>
                    <BookFormField/>
                </EditModal>
            </tr>
        );
    }
}

export default Book;
