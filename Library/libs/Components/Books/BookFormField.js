import React, {Component,} from 'react';
import {ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import Select from 'react-select';

class BookFormField extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fields: {
                name: this.props.values.hasOwnProperty('Name') ? this.props.values.Name : "",
                vendor: this.props.values.hasOwnProperty('Vendor') ? this.props.values.Vendor : "",
                author: this.props.values.hasOwnProperty('Author') ? this.props.values.Author : "",
                year: this.props.values.hasOwnProperty('Year') ? this.props.values.Year : "",
                bookPublicshingId: this.props.values.hasOwnProperty('BookPublicshingId') ? this.props.values.BookPublicshingId : "",
                pubblishing: this.props.values.hasOwnProperty('Pubblishing') ? this.props.values.Pubblishing : "",
            }
        };
    }

    handleChange(field, e) {
        var fields = this.state.fields;
        var value =
            e ?
                e.target ?
                    e.target.value
                    : e
                : "";
        fields[field] = value;
        this.setState({fields: fields});
        this.props.handleChange(fields)
    }

    handleChangeDropDown(e) {
        var fields = this.state.fields;
        fields['BookPublicshingId'] = e.value;
        this.props.handleChange(fields)
    }


    render() {

        loadPublishing = (input) => {
            var url = this.props.controllerName + '/GetBPublishingDropDown';
            var params = {filter: input};
            return fetch(url,
                {
                    method: "POST",
                    body: params
                })
                .then((response) => {
                    return response.json();
                }).then((json) => {
                    return {options: json};
                });
        };

        return (
            <FormGroup>
                <ControlLabel>Название книги</ControlLabel>
                <FormControl
                    type="text"
                    value={this.state.fields.name}
                    placeholder="введите название книги"
                    onChange={this.handleChange.bind(this, 'name')}
                />
                <ControlLabel>Автор книги</ControlLabel>
                <FormControl
                    type="text"
                    value={this.state.fields.author}
                    placeholder="введите автора"
                    onChange={this.handleChange.bind(this, 'author')}
                />
                <ControlLabel>Артикул книги</ControlLabel>
                <FormControl
                    type="text"
                    value={this.state.fields.vendor}
                    placeholder="введите артикул"
                    onChange={this.handleChange.bind(this, 'vendor')}
                />
                <ControlLabel>Год издания</ControlLabel>
                <FormControl
                    type="text"
                    value={this.state.fields.year}
                    placeholder="введите год издания"
                    onChange={this.handleChange.bind(this, 'year')}
                />

                <Select.Async
                    value={this.state.fields.BookPublicshingId}
                    options={this.state.testData}
                    loadOptions={getOptions}
                    onChange={this.handleChangeDropDown.bind(this)}
                />

            </FormGroup>
        );
    }
}

export default BookFormField;
