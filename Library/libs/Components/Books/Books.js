import React, {Component} from 'react';
import {Container} from 'flux/utils';
import AppAction from "../../AppAction";
import CreateModal from "../Base/CreateModal";
import {ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import BookStore from "../../Stores/BookStore";
import Book from "./Book";
import BookFormField from "./BookFormField";

class Books extends Component {

    componentDidMount() {
        AppAction.List(this.state.controllerName, this.state.search);
    }

    handleSearch(e) {
        var value =
            e ?
                e.target ?
                    e.target.value
                    : e
                : "";

        var params =  this.state.params;
        params['search']= value;
        AppAction.List(this.state.controllerName, {search:value});
    }

    getSearch() {
        return this.state.params.search;
    }

    render() {
        let books = this.state.books.map((reader) => {
            return <Book key={reader.id} {...reader}
                           search={this.getSearch.bind(this)}
                           controllerName={this.state.controllerName}/>
        });

        let style = {
            paddingBottom: '10px'
        };

        return (
            <div className='panel-body'>
                <div className='table-responsive'>
                    <FormGroup controlId="formInlineName">
                        <ControlLabel>Поиск</ControlLabel>
                        <FormControl type="text"
                                     value={this.state.params.search}
                                     onChange={this.handleSearch.bind(this)}/>
                    </FormGroup>
                    <div style={style}>
                        <button className="btn btn-primary"
                                onClick={() => this.refs.modalWindow.handleOpen(this.state.search)}>
                            Добавить книгу
                        </button>
                    </div>
                    <CreateModal ref="modalWindow"
                                 buttonAction="Сохранить"
                                 title="Создать книгу"
                                 controllerName={this.state.controllerName}
                                 search={this.getSearch.bind(this)}
                    >
                        <BookFormField/>
                    </CreateModal>
                    <table className='table table-striped'>
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th>Артикул</th>
                            <th>Автор</th>
                            <th>Автор</th>
                            <th>Год издания</th>
                        </tr>
                        </thead>
                        <tbody>
                        {books}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

Books.getStores = () => ([BookStore]);
Books.calculateState = (prevState) => ({
        books: BookStore.getState(),
        controllerName: "Book",
        params: {}
    }
);

export default Container.create(Books);