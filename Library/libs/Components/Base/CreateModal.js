import React, {Component, Children, cloneElement} from 'react';
import ModalWindow from "./ModalWindow";
import AppAction from "../../AppAction";

class CreateModal extends Component {

    create() {
        var url = this.props.controllerName + '/create';
        var scope = this;
        var params= {entity:this.state.entity};
        $.post(url, params, function (data) {
            var filter = scope.props.search();
            AppAction.List(scope.props.controllerName, filter);
            scope.refs.modalWindow.handleClose();
        });
    }

    handleChange(value) {
        this.setState({entity: value});
    }

    handleOpen(filter) {
        this.setState({filter: filter});
        this.refs.modalWindow.handleOpen();
    }

    render() {
        var children = Children.map(this.props.children,
            (child) => cloneElement(child, {
                handleChange: this.handleChange.bind(this),
                values: {}
            })
        );
        return (
            <ModalWindow ref="modalWindow"
                         buttonAction={this.props.buttonAction}
                         title={this.props.title}
                         handleSubmit={this.create.bind(this)}>
                {children}
            </ModalWindow>
        );
    }
}

export default CreateModal;