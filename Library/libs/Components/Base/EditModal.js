import React, {Component, Children, cloneElement} from 'react';
import ModalWindow from "./ModalWindow";
import AppAction from "../../AppAction";

class EditModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            values: {}
        };
    }

    editReader() {
        if (!this.state.entity)
            this.refs.modalWindow.handleClose();
        
        var url = this.props.controllerName + '/edit';
        var scope = this;
        var entity= this.state.entity;
        entity['id'] = this.props.Id;
        var params = {entity: entity};
        $.post(url, params, function (data) {
            var filter = scope.props.getFilter();
            AppAction.List(scope.props.controllerName, filter);
            scope.refs.modalWindow.handleClose();
        });
    }

    handleChange(value) {
        this.setState({entity: value});
    }

    handleOpen() {
        var url = this.props.controllerName + '/detail';
        var params = {id: this.props.Id};
        var scope = this;
        $.get(url, params, function (data) {
            scope.setState({values: data});
            scope.refs.modalWindow.handleOpen();
        });
    }

    render() {
        const children = Children.map(this.props.children,
            (child) => cloneElement(child, {
                handleChange: this.handleChange.bind(this),
                values: this.state.values
            })
        );
        return (
            <ModalWindow ref="modalWindow"
                         buttonAction={this.props.buttonAction}
                         title={this.props.title}
                         handleSubmit={this.editReader.bind(this)}>
                {children}
            </ModalWindow>
        );
    }
}

export default EditModal;