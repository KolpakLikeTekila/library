import React, { Component } from 'react';
import {Button, Modal} from "react-bootstrap";

class ModalWindow extends Component {

    constructor(props) {
        super(props);
        this.state = { showModal: false };
    }

    handleOpen() {
        this.setState({showModal: true});
    }

    handleClose() {
        this.setState({showModal: false});
    }

    render() {
        return (
            <Modal show={this.state.showModal} onHide={this.handleClose.bind(this)}>
                <Modal.Header closeButton>
                    <Modal.Title>{this.props.title}</Modal.Title>
                </Modal.Header>
                    <Modal.Body>
                        {this.props.children}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.props.handleSubmit.bind(this)}>{this.props.buttonAction}</Button>
                        <Button onClick={this.handleClose.bind(this)}>Отменить</Button>
                    </Modal.Footer>
            </Modal>
        );
    }
}

export default ModalWindow;