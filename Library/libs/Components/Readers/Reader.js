import React, {Component,} from 'react';
import ReaderFormField from "./ReaderFormField";
import EditModal from "../Base/EditModal";
import AppAction from "../../AppAction";

class Reader extends Component {

    removeRider() {
        var url = this.props.controllerName + '/remove';
        var params = {id: this.props.Id};
        var scope = this;
        $.post(url, params, function (data) {
            var search = scope.props.search();
            AppAction.List(scope.props.controllerName, search);
        });
    }

    render() {
        return (
            <tr>
                <td>{this.props.Name}</td>
                <td>{this.props.DateBirth}</td>
                <td>
                    <button title="Edit" className="btn btn-primary" onClick={() => this.refs.modalWindow.handleOpen()}>
                        Изменить
                    </button>
                    <button title="Delete" className="btn btn-danger" onClick={this.removeRider.bind(this)}>
                        Удалить
                    </button>
                </td>
                <EditModal ref="modalWindow"
                           buttonAction="Изменить"
                           title="Редактор читателя"
                           controllerName={this.props.controllerName}
                           Id={this.props.Id}
                           getFilter={this.props.search}>
                    <ReaderFormField/>
                </EditModal>
            </tr>
        );
    }
}

export default Reader;
