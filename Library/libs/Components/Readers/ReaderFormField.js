import React, {Component} from 'react';
import {ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import DatePicker from "react-bootstrap-date-picker";

class ReaderFormField extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fields: {
                name: this.props.values.hasOwnProperty('Name') ? this.props.values.Name : "",
                dateBirth: this.props.values.hasOwnProperty('DateBirth') ? this.props.values.DateBirth : ""
            }
        };
    }

    handleChange(field, e) {
        var fields = this.state.fields;
        var value =
            e ?
                e.target ?
                    e.target.value
                    : e
                : "";
        fields[field] = value;
        this.props.handleChange(fields)
    }

    render() {
        var style = {width: "280px"};
        return (
            <FormGroup>
                <ControlLabel>ФИО</ControlLabel>
                <FormControl
                    type="text"
                    value={this.state.fields.name}
                    placeholder="введите имя"
                    onChange={this.handleChange.bind(this, 'name')}
                />
                <ControlLabel>Дата рождения</ControlLabel>
                <div style={style}>
                    <DatePicker
                        value={this.state.fields.dateBirth}
                        onChange={this.handleChange.bind(this, 'dateBirth')}
                    />
                </div>
            </FormGroup>
        );
    }
}

export default ReaderFormField;
