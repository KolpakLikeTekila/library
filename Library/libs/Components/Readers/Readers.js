import React, {Component} from 'react';
import {Container} from 'flux/utils';
import ReaderStore from "../../Stores/ReaderStore";
import Reader from "./Reader";
import AppAction from "../../AppAction";
import ReaderFormField from "./ReaderFormField";
import CreateModal from "../Base/CreateModal";
import {ControlLabel, FormControl, FormGroup} from "react-bootstrap";

class Readers extends Component {

    componentDidMount() {
        AppAction.List(this.state.controllerName, this.state.search);
    }

    handleSearch(e) {
        var value =
            e ?
                e.target ?
                    e.target.value
                    : e
                : "";

        var params =  this.state.params;
        params['search']= value;
        AppAction.List(this.state.controllerName, {search:value});
    }

    getSearch() {
        return this.state.params.search;
    }

    render() {
        let readers = this.state.readers.map((reader) => {
            return <Reader key={reader.id} {...reader}
                           search={this.getSearch.bind(this)}
                           controllerName={this.state.controllerName}/>
        });

        let style = {
            paddingBottom: '10px'
        };

        return (
            <div className='panel-body'>
                <div className='table-responsive'>
                    <FormGroup controlId="formInlineName">
                        <ControlLabel>Поиск</ControlLabel>
                        <FormControl type="text"
                                     value={this.state.params.search}
                                     onChange={this.handleSearch.bind(this)}/>
                    </FormGroup>
                    <div style={style}>
                        <button className="btn btn-primary"
                                onClick={() => this.refs.modalWindow.handleOpen(this.state.search)}>
                            Добавить читателя
                        </button>
                    </div>
                    <CreateModal ref="modalWindow"
                                 buttonAction="Сохранить"
                                 title="Создать читателя"
                                 controllerName={this.state.controllerName}
                                 search={this.getSearch.bind(this)}
                    >
                        <ReaderFormField/>
                    </CreateModal>
                    <table className='table table-striped'>
                        <thead>
                        <tr>
                            <th>ФИО</th>
                            <th>Дата рождения</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {readers}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

Readers.getStores = () => ([ReaderStore]);
Readers.calculateState = (prevState) => ({
        readers: ReaderStore.getState(),
        controllerName: "reader",
        params: {}
    }
);

export default Container.create(Readers);