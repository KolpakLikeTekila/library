import React, {Component} from 'react';
import {ControlLabel, FormControl, FormGroup} from "react-bootstrap";

class BookPublishingFormField extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fields: {
                name: this.props.values.hasOwnProperty('Name') ? this.props.values.Name : "",
                shortName: this.props.values.hasOwnProperty('ShortName') ? this.props.values.ShortName : "",
                adress: this.props.values.hasOwnProperty('Adress') ? this.props.values.Adress : ""
            }
        };
    }

    handleChange(field, e) {
        var fields = this.state.fields;
        var value =
            e ?
                e.target ?
                    e.target.value
                    : e
                : "";
        fields[field] = value;
        this.props.handleChange(fields)
    }

    render() {
        return (
            <FormGroup>
                <ControlLabel>Полное наименование</ControlLabel>
                <FormControl
                    type="text"
                    value={this.state.fields.name}
                    placeholder="введите название издательства"
                    onChange={this.handleChange.bind(this, 'name')}
                />
                <ControlLabel>Сокращенное наименование</ControlLabel>
                <FormControl
                    type="text"
                    value={this.state.fields.shortName}
                    placeholder="введите сокращенное наименование"
                    onChange={this.handleChange.bind(this, 'shortName')}
                />
                <ControlLabel>Адрес издательства</ControlLabel>
                <FormControl
                    type="text"
                    value={this.state.fields.adress}
                    placeholder="введите адрес"
                    onChange={this.handleChange.bind(this, 'adress')}
                />
            </FormGroup>
        );
    }
}

export default BookPublishingFormField;
