import React, {Component} from 'react';
import {Container} from 'flux/utils';
import AppAction from "../../AppAction";
import CreateModal from "../Base/CreateModal";
import {ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import BookPublishingStore from "../../Stores/BookPublishingStore";
import BookPublishing from "./BookPublishing";
import BookPublishingFormField from "./BookPublishingFormField";

class BookPublishings extends Component {

    componentDidMount() {
        AppAction.List(this.state.controllerName, this.state.search);
    }

    handleSearch(e) {
        var value =
            e ?
                e.target ?
                    e.target.value
                    : e
                : "";

        var params =  this.state.params;
        params["search"]= value;
        AppAction.List(this.state.controllerName, {search:value});
    }

    getSearch() {
        return this.state.params.search;
    }

    render() {
        let bookPublishings = this.state.bookPublishings.map((bookPublishing) => {
            return <BookPublishing key={bookPublishing.id} {...bookPublishing}
                           search={this.getSearch.bind(this)}
                           controllerName={this.state.controllerName}/>
        });

        let style = {
            paddingBottom: '10px'
        };

        return (
            <div className='panel-body'>
                <div className='table-responsive'>
                    <FormGroup controlId="formInlineName">
                        <ControlLabel>Поиск</ControlLabel>
                        <FormControl type="text"
                                     value={this.state.params.search}
                                     onChange={this.handleSearch.bind(this)}/>
                    </FormGroup>
                    <div style={style}>
                        <button className="btn btn-primary"
                                onClick={() => this.refs.modalWindow.handleOpen(this.state.search)}>
                            Добавить издательство
                        </button>
                    </div>
                    <CreateModal ref="modalWindow"
                                 buttonAction="Сохранить"
                                 title="Создать издательство"
                                 controllerName={this.state.controllerName}
                                 search={this.getSearch.bind(this)}>
                        <BookPublishingFormField/>
                    </CreateModal>
                    <table className='table table-striped'>
                        <thead>
                        <tr>
                            <th>Полное наименование</th>
                            <th>Сокращенное наименование</th>
                            <th>Адрес издательства</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {bookPublishings}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

BookPublishings.getStores = () => ([BookPublishingStore]);
BookPublishings.calculateState = (prevState) => ({
        bookPublishings: BookPublishingStore.getState(),
        controllerName: "BookPublishing",
        params: {search:""}
    }
);

export default Container.create(BookPublishings);