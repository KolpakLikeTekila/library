﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Library.Entity;

namespace Library
{
    public class DbInitializer : DropCreateDatabaseAlways<DBContext>
    {
        protected override void Seed(DBContext context)
        {
            var readers = new List<Reader>
            {
                new Reader
                {
                    Id = 1,
                    DateBirth = DateTime.Now,
                    Name = "123"
                },
                new Reader
                {
                    Id = 2,
                    DateBirth = DateTime.Now,
                    Name = "qwe"
                }
            };
            
            

            context.Set<Reader>().AddRange(readers);
            base.Seed(context);
        }
    }
}