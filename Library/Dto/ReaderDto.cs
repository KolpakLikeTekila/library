﻿namespace Library.Dto
{
    /// <summary> Дто читателя </summary>
    public sealed class ReaderDto
    {
        /// <summary> идентификатор </summary>
        public long Id { get; set; }

        /// <summary> Имя читателя </summary>
        public string Name { get; set; }

        /// <summary> Дата рождения читателя </summary>
        public string DateBirth { get; set; }
    }
}