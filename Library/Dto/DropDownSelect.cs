﻿namespace Library.Dto
{
    public class DropDownSelect
    {
        public long value { get; set; }
        public string label { get; set; }
    }
}