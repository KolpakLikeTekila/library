﻿namespace Library.Dto
{
    /// <summary> Дто книги </summary>
    public sealed class BookDto
    {
        /// <summary> идентификатор </summary>
        public long Id { get; set; }

        /// <summary> Имя читателя </summary>
        public string Name { get; set; }

        /// <summary> Артикул </summary>
        public string Vendor { get; set; }

        /// <summary> Автор </summary>
        public string Author { get; set; }

        /// <summary>  Год издания </summary>
        public string Year { get; set; }

        /// <summary> Издательство </summary>
        public string Publicshin { get; set; }
        
        /// <summary> Идентификатор издательства </summary>
        public long BookPublicshingId { get; set; }
    }
}