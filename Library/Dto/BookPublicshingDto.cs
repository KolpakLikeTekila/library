﻿namespace Library.Dto
{
    /// <summary> Дто издателя </summary>
    public sealed class BookPublicshingDto
    {
        /// <summary> идентификатор </summary>
        public long Id { get; set; }

        /// <summary> Полное имя издателя </summary>
        public string Name { get; set; }
        
        /// <summary> Сокращенное имя издателя </summary>
        public string ShortName { get; set; }
        
        /// <summary> Адрем издателя </summary>
        public string Adress { get; set; }
    }
}