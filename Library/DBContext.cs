﻿using System.Data.Entity;
using Library.Entity;

namespace Library
{
    public class DBContext : DbContext
    {
        public DBContext() : base("LibraryDB")
        {
        }
        
        public DbSet<Reader> Readers { get; set; }
        public DbSet<BookPublicshing> BookPublicshings { get; set; }
        public DbSet<Book> Books { get; set; }
    }
}